<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Check Out Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>0ca432e1-99b5-4cc1-a82a-ab67da85e51a</testSuiteGuid>
   <testCaseLink>
      <guid>18621bf7-9426-4d75-9049-7615daa21bd7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>''</defaultValue>
         <description></description>
         <id>b7c32e39-b5d4-4efe-bcdc-f71e6ca3b600</id>
         <masked>false</masked>
         <name>firstName</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/Check Out Test/CO-002_Check Out Test</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a9a012d7-9d46-4206-8062-bef35947c8c2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Raw Data/Data Driven</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>a9a012d7-9d46-4206-8062-bef35947c8c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>first name</value>
         <variableId>b7c32e39-b5d4-4efe-bcdc-f71e6ca3b600</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>a9a012d7-9d46-4206-8062-bef35947c8c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>second name</value>
         <variableId>4ef87c89-6524-420b-a087-be7fd893687d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>a9a012d7-9d46-4206-8062-bef35947c8c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>postal code</value>
         <variableId>a2f86491-99ff-4816-b309-97c3c3fd6d97</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
