<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>9e0f5d6d-6c7d-4de7-b3fd-e4f258d0b370</testSuiteGuid>
   <testCaseLink>
      <guid>22d65893-d922-47f4-8f9e-33dba167f4d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Test/LG-003_Login Test</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0e720e1e-ed2d-4f21-9b82-66528248408b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Raw Data/Data Driven</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>0e720e1e-ed2d-4f21-9b82-66528248408b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>b4b0bbfa-ac52-43d7-964c-b0dec185829f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0e720e1e-ed2d-4f21-9b82-66528248408b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>1cb99fde-7e0c-4871-97b2-6820c5834403</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
