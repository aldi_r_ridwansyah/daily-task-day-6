<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Epic sadface Username and password do not match any user in this service</name>
   <tag></tag>
   <elementGuidId>5f4adb7a-1724-459d-a1e0-8ea731d6610a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='login_button_container']/div/form/div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.error-message-container.error</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e3bfc1c4-0ba5-42ad-8d3e-e68548de3d77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>error-message-container error</value>
      <webElementGuid>bb52c840-f2cb-4111-a838-07472d9a2ac7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Epic sadface: Username and password do not match any user in this service</value>
      <webElementGuid>c5ea6173-e7e8-440b-ae50-26045502ce2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login_button_container&quot;)/div[@class=&quot;login-box&quot;]/form[1]/div[@class=&quot;error-message-container error&quot;]</value>
      <webElementGuid>be33f357-feab-44e2-9c59-25d9dcd20db8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='login_button_container']/div/form/div[3]</value>
      <webElementGuid>5a17c521-86ba-4814-8ac8-ccb30507eece</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]</value>
      <webElementGuid>5184f869-1259-4ae2-aaf0-40aa783b7455</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Epic sadface: Username and password do not match any user in this service' or . = 'Epic sadface: Username and password do not match any user in this service')]</value>
      <webElementGuid>a552fa4c-180c-4be0-9653-f974ad537b80</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
